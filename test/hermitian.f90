program test_hermitian

  use linalg_kinds, only : wp
  use linalg_matrices_diagonalize, only : diagonalizehermitian

  implicit none

  integer, parameter :: dimen = 3
  complex(wp) :: B(dimen,dimen)
  real(wp) :: eigenvalues(dimen)
  complex(wp) :: eigenvectors(dimen,dimen)
  integer :: kont

  integer :: i
  real(wp) :: testsum

  B(1,1) = 1.0E0_wp
  B(1,2) = 0.0E0_wp
  B(1,3) = 2.0E0_wp
  B(2,1) = 0.0E0_wp
  B(2,2) = 4.0E0_wp
  B(2,3) = 5.0E0_wp
  B(3,1) = 2.0E0_wp
  B(3,2) = 5.0E0_wp
  B(3,3) = 3.0E0_wp
  B(1,3) = B(1,3) + (0,1) * B(2,2)
  B(3,1) = B(3,1) - (0,1) * B(2,2)
  B(1,2) = B(1,2) + (0,1) * B(3,3)
  B(2,1) = B(2,1) - (0,1) * B(3,3)
  B(2,3) = B(2,3) + (0,1) * B(1,1)
  B(3,2) = B(3,2) - (0,1) * B(1,1)

  kont = 0

  call diagonalizehermitian(  &
    B,  &
    eigenvalues,  &
    eigenvectors,  &
    kont)

  testsum = 0.0E0_wp
  do i=1,3
    testsum = testsum + sum(abs(matmul(B,eigenvectors(i,:)) -  &
      eigenvectors(i,:) * eigenvalues(i)))
  enddo

  write(*,*) "Testing diagonalizehermitian..."
  if (testsum < 1.0E-8_wp) then
    write(*,*) "  Test successful:", testsum
  else
    write(*,*) "  Test failed:", testsum
  endif

end program test_hermitian
