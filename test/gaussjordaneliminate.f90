program test_gaussjordaneliminate

  use linalg_kinds, only : wp
  use linalg_solvers_linear, only : gaussjordaneliminate

  ! Example from wikipedia
  integer, parameter :: n = 3
  real(wp) :: a(n,n)
  real(wp) :: b(n)
  real(wp) :: y(n)
  integer :: raiser = 0

  integer :: i
  real(wp) :: testsum = 0.0e0_wp

  a(1,1) = 2.0e0_wp
  a(1,2) = 1.0e0_wp
  a(1,3) = -1.0e0_wp
  a(2,1) = -3.0e0_wp
  a(2,2) = -1.0e0_wp
  a(2,3) = 2.0e0_wp
  a(3,1) = -2.0e0_wp
  a(3,2) = 1.0e0_wp
  a(3,3) = 2.0e0_wp

  b(1) = 8.0e0_wp
  b(2) = -11.0e0_wp
  b(3) = -3.0e0_wp

  write(*,*) "Testing gaussjordaneliminate..."
  call gaussjordaneliminate(n, a, b, y, raiser)
  if (raiser == 0) then
    write(*,*) "    ", "y = ", y
    write(*,*) "    ", "Is a * y = b?"
    do i=1,3
      write(*,*) "    ", sum(a(i,:) * y(:)), " == ", b(i)
      testsum = testsum + sum(a(i,:) * y(:)) - b(i)
    end do
  else
    write(*,*) "Failed. Status: raiser = ", raiser
  end if
  if (testsum < 1.0e-8_wp) then
    write(*,*) "  Test sucessful:", testsum
  else
    write(*,*) "  Test failed:", testsum
  end if

end program test_gaussjordaneliminate
