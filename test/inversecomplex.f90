program test_inversecomplex

  use linalg_kinds, only : wp
  use linalg_matrices_inverse, only : inversecomplex

  implicit none

  integer, parameter :: dimen = 3
  complex(wp) :: A(dimen,dimen)
  complex(wp) :: Ainv(dimen,dimen)
  complex(wp) :: one(dimen,dimen)
  real(wp) :: testsum

  A(1,1) = (1,0) * 1.0E0_wp
  A(1,2) = (1,0) * 0.0E0_wp
  A(1,3) = -(1,0) * 2.0E0_wp
  A(2,1) = (1,0) * 0.0E0_wp
  A(2,2) = (1,0) * 24.0E0_wp
  A(2,3) = (1,0) * 15.0E0_wp
  A(3,1) = (1,0) * 2.0E0_wp
  A(3,2) = -(1,0) * 5.0E0_wp
  A(3,3) = (1,0) * 3.0E0_wp

  A(1,1) = A(1,1) - (0,1) * 1.0E0_wp
  A(1,2) = A(1,2) + (0,1) * 0.0E0_wp
  A(1,3) = A(1,3) + (0,1) * 2.0E0_wp
  A(2,1) = A(2,1) + (0,1) * 0.0E0_wp
  A(2,2) = A(2,2) - (0,1) * 4.0E0_wp
  A(2,3) = A(2,3) - (0,1) * 15.0E0_wp
  A(3,1) = A(3,1) + (0,1) * 2.0E0_wp
  A(3,2) = A(3,2) + (0,1) * 5.0E0_wp
  A(3,3) = A(3,3) + (0,1) * 3.0E0_wp

  Ainv = inversecomplex(A)

  one = abs(matmul(A, Ainv))
  testsum = sum(real(one)) + sum(aimag(one)) - dimen

  write(*,*) "Testing inversecomplex..."
  if (testsum < 1.0E-8_wp) then
    write(*,*) "  Test successful:", testsum
  else
    write(*,*) "  Test failed:", testsum
  endif

end program test_inversecomplex
