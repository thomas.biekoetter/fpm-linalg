program test_realsymmetric

  use linalg_kinds, only : wp
  use linalg_matrices_diagonalize, only : diagonalizerealsymmetric

  implicit none

  integer, parameter :: dimen = 3
  real(wp) :: A(dimen,dimen)
  real(wp) :: eigenvalues(dimen)
  real(wp) :: eigenvectors(dimen,dimen)
  integer :: kont
  integer :: order

  integer :: i
  real(wp) :: testsum

  A(1,1) = 1.0E0_wp
  A(1,2) = 0.0E0_wp
  A(1,3) = 2.0E0_wp
  A(2,1) = 0.0E0_wp
  A(2,2) = 4.0E0_wp
  A(2,3) = 5.0E0_wp
  A(3,1) = 2.0E0_wp
  A(3,2) = 5.0E0_wp
  A(3,3) = 3.0E0_wp

  order = 1
  kont = 0

  call diagonalizerealsymmetric(  &
    A,  &
    eigenvalues,  &
    eigenvectors,  &
    order)

  testsum = 0.0E0_wp
  do i=1,3
    testsum = testsum + sum(abs(matmul(A,eigenvectors(i,:)) -  &
      eigenvectors(i,:) * eigenvalues(i)))
  enddo

  write(*,*) "Testing diagonalizerealsymmetric..."
  if (testsum < 1.0E-8_wp) then
    write(*,*) "  Test successful:", testsum
  else
    write(*,*) "  Test failed:", testsum
  endif

end program test_realsymmetric
