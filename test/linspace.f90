program test_linspace

  use linalg_kinds, only : wp
  use linalg_create_arrays, only : linspace

  implicit none

  real(wp) :: a = 3.0e0_wp
  real(wp) :: b = 43.0e0_wp
  integer, parameter :: n = 10
  real(wp) :: y(n)

  integer :: i

  write(*,*) "Testing linspace..."
  y = linspace(a, b, n)
  do i=1,n
    write(*,"(a,I2,a,5E13.6)") "    y(", i, ") = ", y(i)
  end do
  if ((y(1) == a) .and. (y(n) == b)) then
    write(*,*) "  Test sucessful."
  else
    write(*,*) "  Test failed."
  end if

end program test_linspace
