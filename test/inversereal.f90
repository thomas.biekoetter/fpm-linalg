program test_inversereal

  use linalg_kinds, only : wp
  use linalg_matrices_inverse, only : inversereal

  implicit none

  integer, parameter :: dimen = 3
  real(wp) :: A(dimen,dimen)
  real(wp) :: Ainv(dimen,dimen)
  real(wp) :: testsum

  A(1,1) = 1.0E0_wp
  A(1,2) = 0.0E0_wp
  A(1,3) = -2.0E0_wp
  A(2,1) = 0.0E0_wp
  A(2,2) = 24.0E0_wp
  A(2,3) = 15.0E0_wp
  A(3,1) = 2.0E0_wp
  A(3,2) = -5.0E0_wp
  A(3,3) = 3.0E0_wp

  Ainv = inversereal(A)

  testsum = sum(abs(matmul(A, Ainv))) - dimen

  write(*,*) "Testing inversereal..."
  if (testsum < 1.0E-8_wp) then
    write(*,*) "  Test successful:", testsum
  else
    write(*,*) "  Test failed:", testsum
  endif

end program test_inversereal
