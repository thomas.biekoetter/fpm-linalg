program test_realsingularvaluedecomp

  use linalg_kinds, only : wp
  use linalg_matrices_diagonalize, only : svd_square

  implicit none

  integer, parameter :: dimen = 3
  real(wp) :: C(dimen,dimen)
  real(wp) :: eigenvalues(dimen)
  real(wp) :: zel(dimen,dimen)
  real(wp) :: zer(dimen,dimen)
  integer :: kont
  integer :: order

  integer :: i
  real(wp) :: test1(dimen,dimen)
  real(wp) :: test2(dimen,dimen)
  real(wp) :: test3(dimen,dimen)
  real(wp) :: test4(dimen,dimen)
  real(wp) :: testsum
  real(wp) :: diag(dimen,dimen)

  C(1,1) = 1.0E0_wp
  C(1,2) = 0.5E0_wp
  C(1,3) = 2.0E0_wp
  C(2,1) = -10.0E0_wp
  C(2,2) = 4.0E0_wp
  C(2,3) = 5.0E0_wp
  C(3,1) = -2.5E0_wp
  C(3,2) = 15.0E0_wp
  C(3,3) = 3.0E0_wp

  kont = 0
  order = 1

  call svd_square(  &
    dimen,  &
    C,  &
    eigenvalues,  &
    zel,  &
    zer,  &
    kont,  &
    order)

  test1 = matmul(zel, transpose(zel))
  test2 = matmul(zer, transpose(zer))
  diag = 0.0e0_wp
  do i=1,dimen
    diag(i,i) = eigenvalues(i)
  enddo
  test3 = matmul(transpose(zel), matmul(diag, zer))
  test4 = matmul(zel, matmul(C, transpose(zer)))
  testsum = 0.0E0_wp
  testsum = testsum + abs(sum(test1) - dimen)
  testsum = testsum + abs(sum(test2) - dimen)
  testsum = testsum + abs(sum(C - test3))
  testsum = testsum + abs(sum(diag - test4))

  write(*,*) "Testing singular value decomposition for real matrices..."
  if (testsum < 1.0E-8_wp) then
    write(*,*) "  Test successful:", testsum
  else
    write(*,*) "  Test failed:", testsum
  endif

end program test_realsingularvaluedecomp
