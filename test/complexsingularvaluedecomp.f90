program test_complexsingularvaluedecomp

  use linalg_kinds, only : wp
  use linalg_matrices_diagonalize, only : svd_complex, svd
  use linalg_print_matrices, only : print_matrix
  use linalg_checks_realness, only : isreal_array

  implicit none

  integer, parameter :: dimen1 = 3
  integer, parameter :: dimen2 = 4
  complex(wp) :: C(dimen1,dimen2)
  real(wp) :: eigenvalues(dimen1)
  complex(wp) :: zel(dimen1,dimen1)
  complex(wp) :: zer(dimen2,dimen2)
  integer :: kont
  integer :: order
  real(wp) :: B(dimen1,dimen2)
  real(wp) :: eigenvaluesB(dimen1)
  real(wp) :: zelB(dimen1,dimen1)
  real(wp) :: zerB(dimen2,dimen2)

  integer :: i
  complex(wp) :: test1(dimen1,dimen1)
  complex(wp) :: test2(dimen2,dimen2)
  complex(wp) :: test3(dimen1,dimen2)
  real(wp) :: testsum
  real(wp) :: diag(dimen1,dimen2)

  C(1,1) = (1,1) * 1.0E0_wp
  C(1,2) = 0.5E0_wp
  C(1,3) = 2.0E0_wp
  C(1,4) = (0,1) * 2.0E0_wp
  C(2,1) = (0,1) * (-10.0E0_wp)
  C(2,2) = 4.0E0_wp
  C(2,3) = (0,1) * 5.0E0_wp
  C(2,4) = (1,1) * 5.0E0_wp
  C(3,1) = (1,1) * (-2.5E0_wp)
  C(3,2) = 15.0E0_wp
  C(3,3) = 3.0E0_wp
  C(3,4) = (1,1) * 7.0E0_wp

  if (isreal_array(C)) then
    write(*,*) "Use a complex matrix."
    call exit
  end if

  kont = 0
  order = 1

  call svd_complex(  &
    C,  &
    eigenvalues,  &
    zel,  &
    zer)

  test1 = matmul(zel, conjg(transpose(zel)))
  write(*,*) "real(U Udagger) ="
  call print_matrix(real(test1))
  write(*,*) "imag(U Udagger) ="
  call print_matrix(aimag(test1))
  test2 = matmul(zer, conjg(transpose(zer)))
  write(*,*) "real(V Vdagger) ="
  call print_matrix(real(test2))
  write(*,*) "imag(V Vdagger) ="
  call print_matrix(aimag(test2))
  diag = 0.0e0_wp
  do i=1,dimen1
    diag(i,i) = eigenvalues(i)
  enddo
  write(*,*) "Sigma = "
  call print_matrix(diag)
  write(*,*) "Get back original matrix?"
  write(*,*)
  write(*,*) "real(C) ="
  test3 = matmul(zel, matmul(diag, conjg(transpose(zer))))
  call print_matrix(real(test3))
  write(*,*) "imag(C) ="
  call print_matrix(real(test3))
  write(*,*)
  write(*,*) "Testing singular value decomposition for complex matrices..."
  testsum = sum(abs(test3) - abs(C))
  if (testsum < 1.0E-8_wp) then
    write(*,*) "  Test successful:", testsum
  else
    write(*,*) "  Test failed:", testsum
  endif

  write(*,*) "Testing if singular value decomposition is consistent with svd for real matrices..."
  C = real(C) + (0,1) * 1.0e-10_wp
  B = real(C)
  call svd_complex(  &
    C,  &
    eigenvalues,  &
    zel,  &
    zer)
  call svd(  &
    B,  &
    eigenvaluesB,  &
    zelB,  &
    zerB)
  testsum = sum(eigenvalues - eigenvaluesB)
  if (testsum < 1.0E-8_wp) then
    write(*,*) "  Test successful:", testsum
  else
    write(*,*) "  Test failed:", testsum
  endif

end program test_complexsingularvaluedecomp
