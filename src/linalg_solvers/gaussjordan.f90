module linalg_solvers_linear

  use linalg_kinds, only : wp

  implicit none

  private

  public :: gaussjordaneliminate

contains

  subroutine gaussjordaneliminate(N, A, B, y, raiser)

    integer, intent(in) :: N
    real(wp), intent(in) :: A(N,N)
    real(wp), intent(in) :: B(N)
    real(wp), intent(out) :: y(N)
    integer, intent(out) :: raiser

    integer :: i, j, k
    real(wp) :: aug(N,N+1)
    real(wp) :: mult
    real(wp) :: pivot, swap(N+1)
    integer :: ipivot
    logical :: haszerorow

    raiser = 0

    do i=1,N
      do j=1,N
        aug(j,i) = A(j,i)
      enddo
    enddo
    do i=1,N
      aug(i,N+1) = B(i)
    enddo

    do k=1,N
      do i=1,N
        if(i.ne.k) then
          if (raiser.eq.0) then
            call checkzerorow(aug, N, N+1, haszerorow)
            if (haszerorow) then
              write(*,*) 'Ill-defined system, found flat direction.'
              raiser = 1
            endif
            pivot = aug(k,k)
            ipivot = k
            do j=k+1,N
              if (abs(aug(j,k)).gt.abs(pivot)) then
                pivot = aug(j,k)
                ipivot = j
              endif
            enddo
            if (ipivot.ne.k) then
              swap = aug(k,:)
              aug(k,:) = aug(ipivot,:)
              aug(ipivot,:) = swap
            endif
            mult = aug(i,k) / aug(k,k)
            ! write(*,*) aug(i,k), aug(k,k), mult
            do j=1,N+1
              aug(i,j) = aug(i,j) - aug(k,j) * mult
            enddo
          endif
        endif
      enddo
    enddo

    if (raiser.eq.0) then
      do i=1,N
        y(i) = aug(i,N+1) / aug(i,i)
      enddo
    endif

  end subroutine gaussjordaneliminate

  subroutine checkzerorow(a, n1, n2, y)

    integer, intent(in) :: n1, n2
    real(wp), intent(in) :: a(n1,n2)
    logical, intent(out) :: y

    integer :: i, j
    real(wp) :: numzero = 1.0E-12_wp
    logical, dimension(n1) :: yr

    do i=1,n1
      yr(i) = .true.
      do j=1,n2
        if (abs(a(i,j)).gt.numzero) then
          yr(i) = .false.
        endif
      enddo
    enddo

    y = .false.
    do i=1,n1
      if (yr(i)) then
        y = .true.
      endif
    enddo

  end subroutine checkzerorow

  subroutine checkstepnonzero(s, N, y)

    integer, intent(in) :: N
    real(wp), intent(in) :: s(N)
    logical, intent(out) :: y

    integer :: i

    y = .false.
    do i=1,N
      if (abs(s(i)).gt.1.0E-12_wp) then
        y = .true.
        exit
      endif
    enddo

  end subroutine checkstepnonzero

end module linalg_solvers_linear
