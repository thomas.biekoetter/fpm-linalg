module linalg_matrices_diagonalize

  use linalg_kinds, only : wp
  use linalg_matrices_inverse, only : inversecomplex

  implicit none

  private ::  &
    jacobi

  public ::  &
    svd_square,  &
    diagonalizehermitian,  &
    diagonalizerealsymmetric,  &
    svd,  &
    svd_complex

contains

  ! Definition: matrix = zel Sigma zer^dagger
  subroutine svd_complex(  &
    matrix,  &
    eigenvalues,  &
    zel,  &
    zer)

    complex(wp), intent(in) :: matrix(:,:)
    real(wp), intent(out) :: eigenvalues(:)
    complex(wp), intent(out) :: zel(:,:)
    complex(wp), intent(out) :: zer(:,:)

    complex(wp), allocatable :: matsq1(:,:)
    complex(wp), allocatable :: matsq2(:,:)
    complex(wp), allocatable :: zel2(:,:)
    complex(wp), allocatable :: zer2(:,:)
    real(wp), allocatable :: eigenvaluesl(:)
    real(wp), allocatable :: eigenvaluesr(:)
    real(wp), allocatable :: diag(:,:)
    real(wp), allocatable :: diagl(:,:)
    real(wp), allocatable :: diagr(:,:)
    real(wp), allocatable :: invdiag(:,:)
    complex(wp), allocatable :: check(:,:)
    complex(wp), allocatable :: checkl(:,:)
    complex(wp), allocatable :: checkr(:,:)
    real(wp), allocatable :: checkm(:,:)
    real(wp) :: eps
    integer :: i
    integer :: m
    integer :: n
    integer :: mn
    integer :: mm
    integer :: order

    order = -1

    m = size(matrix, dim=1)
    n = size(matrix, dim=2)

    mn = min(m, n)
    mm = max(m, n)

    allocate(check(m,n))
    allocate(checkl(m,m))
    allocate(checkr(n,n))
    allocate(checkm(m,m))

    allocate(zel2(m,m))
    allocate(zer2(n,n))

    allocate(eigenvaluesl(m))
    allocate(eigenvaluesr(n))

    allocate(diag(m,n))
    allocate(diagl(m,m))
    allocate(diagr(n,n))
    allocate(invdiag(n,m))

    if ((m /= size(zel, dim=1)) .or. (m /= size(zel, dim=2))) then
      write(*,*) "Shape of zel argument incorrect."
      call exit
    endif

    if ((n /= size(zer, dim=1)) .or. (n /= size(zer, dim=2))) then
      write(*,*) "Shape of zel argument incorrect."
      call exit
    endif

    if (min(m,n) /= size(eigenvalues, dim=1)) then
      write(*,*) "Shape of eigenvalues argument incorrect."
      call exit
    endif

    allocate(matsq1(m,m))
    allocate(matsq2(n,n))

    matsq1 = matmul(conjg(transpose(matrix)), matrix)
    call diagonalizehermitian(  &
      matsq1,  &
      eigenvaluesr,  &
      zer2,  &
      order)
    matsq1 = matmul(conjg(transpose(matrix)), matrix)
    zer2 = transpose(zer2)
    checkr = matmul(inversecomplex(zer2), matmul(matsq1, zer2))
    ! Check if eigensystem solved
    do i=1,n
      if (abs(eigenvaluesr(i) - checkr(i,i)) > 1.0e-10_wp) then
        write(*,*) "Problem with diagonlization of Mdag.M in svd_complex."
        call exit
      end if
    end do
    diagr = 0.0E0_wp
    do i=1,n
      diagr(i,i) = eigenvaluesr(i)
    enddo

    matsq2 = matmul(matrix, conjg(transpose(matrix)))
    call diagonalizehermitian(  &
      matsq2,  &
      eigenvaluesl,  &
      zel2,  &
      order)
    matsq2 = matmul(matrix, conjg(transpose(matrix)))
    zel2 = transpose(zel2)
    checkl = matmul(inversecomplex(zel2), matmul(matsq2, zel2))
    ! Check if eigensystem solved
    do i=1,m
      if (abs(eigenvaluesl(i) - checkl(i,i)) > 1.0e-10_wp) then
        write(*,*) "Problem with diagonlization of M.Mdag in svd_complex."
        call exit
      end if
    end do
    diagl = 0.0E0_wp
    do i=1,m
      diagl(i,i) = eigenvaluesl(i)
    enddo

    ! Check if eigenvalues of Mdag.M and M.Mdag consistent
    do i=1,mn
      eps = abs(eigenvaluesr(i) - eigenvaluesr(i))
      if (eps > 1.0E-8_wp) then
        write(*,*) "Problems with eigenvalues."
        call exit
      endif
    end do
    if (m /= n) then
      do i=mn+1,mm
        if (n > m) then
          eps = abs(eigenvaluesr(i))
        else
          eps = abs(eigenvaluesl(i))
        end if
        if (eps > 1.0E-8_wp) then
          write(*,*) "Problems with eigenvalues."
          call exit
        end if
      end do
    end if

    diag = 0.0E0_wp
    do i=1,mn
      if (n <= m) then
        eigenvalues(i) = sqrt(eigenvaluesr(i))
      else
        eigenvalues(i) = sqrt(eigenvaluesl(i))
      end if
      diag(i,i) = sqrt(eigenvaluesl(i))
    enddo
    invdiag = 0.0e0_wp
    do i=1,mn
      invdiag(i,i) = 1.0e0_wp / diag(i,i)
    end do
    checkm = matmul(diag, invdiag)
    if (sum(abs(checkm)) - m > 1.0e-10_wp) then
      write(*,*) "Problem with inverse of diag matrix."
      call exit
    end if

    ! Check if zer2 and zel2 orthogonal
    checkl = matmul(conjg(transpose(zel2)), zel2)
    eps = abs(sum(checkl) - m)
    if (eps > 1.0E-10) then
      write(*,*) "Left transformation matrix not orthogonal."
      call exit
    endif
    checkr = matmul(conjg(transpose(zer2)), zer2)
    eps = abs(sum(checkr) - n)
    if (eps > 1.0E-10) then
      write(*,*) "Right transformation matrix not orthogonal."
      call exit
    endif

    ! Do not have to switch here because zel with M M^T
!   zel = zel2
    zer = zer2

    ! Get zel from M = U Sig Vdag -> M V SigInv = U
    zel = matmul(matrix, matmul(zer, invdiag))
    ! Check if unitary
    checkl = matmul(conjg(transpose(zel)), zel)
    eps = abs(sum(checkl) - m)
    if (eps > 1.0E-10) then
      write(*,*) "Left transformation matrix not orthogonal."
      call exit
    endif

    ! check if get back original matrix
    check = matmul(zel, matmul(diag, transpose(conjg(zer))))
    eps = sum(check - matrix)
    if (eps > 1.0E-10_wp) then
      write(*,*) "SVD failed. Original matrix not recovered."
      call exit
    endif

  end subroutine svd_complex

  subroutine svd(  &
    matrix,  &
    eigenvalues,  &
    zel,  &
    zer)

    real(wp), intent(in) :: matrix(:,:)
    real(wp), intent(out) :: eigenvalues(:)
    real(wp), intent(out) :: zel(:,:)
    real(wp), intent(out) :: zer(:,:)

    real(wp), allocatable :: matsq1(:,:)
    real(wp), allocatable :: matsq2(:,:)
    real(wp), allocatable :: zel2re(:,:)
    real(wp), allocatable :: zer2re(:,:)
    real(wp), allocatable :: eigenvaluesl(:)
    real(wp), allocatable :: eigenvaluesr(:)
    real(wp), allocatable :: diag(:,:)
    real(wp), allocatable :: diagl(:,:)
    real(wp), allocatable :: diagr(:,:)
    real(wp), allocatable :: check(:,:)
    real(wp), allocatable :: checkl(:,:)
    real(wp), allocatable :: checkr(:,:)
    real(wp) :: eps
    integer :: i
    integer :: m
    integer :: n
    integer :: mn
    integer :: order

    order = 1

    m = size(matrix, dim=1)
    n = size(matrix, dim=2)

    mn = min(m, n)

    allocate(check(m,n))
    allocate(checkl(m,m))
    allocate(checkr(n,n))

    allocate(zel2re(m,m))
    allocate(zer2re(n,n))

    allocate(eigenvaluesl(m))
    allocate(eigenvaluesr(n))

    allocate(diag(m,n))
    allocate(diagl(m,m))
    allocate(diagr(n,n))

    if ((m /= size(zel, dim=1)) .or. (m /= size(zel, dim=2))) then
      write(*,*) "Shape of zel argument incorrect."
      call exit
    endif

    if ((n /= size(zer, dim=1)) .or. (n /= size(zer, dim=2))) then
      write(*,*) "Shape of zel argument incorrect."
      call exit
    endif

    if (min(m,n) /= size(eigenvalues, dim=1)) then
      write(*,*) "Shape of eigenvalues argument incorrect."
      call exit
    endif

    allocate(matsq1(m,m))
    allocate(matsq2(n,n))

    matsq1 = matmul(transpose(matrix), matrix)
    call diagonalizerealsymmetric(  &
      matsq1,  &
      eigenvaluesr,  &
      zer2re,  &
      order)
    diagr = 0.0E0_wp
    do i=1,n
      diagr(i,i) = eigenvaluesr(i)
    enddo

    matsq2 = matmul(matrix, transpose(matrix))
    call diagonalizerealsymmetric(  &
      matsq2,  &
      eigenvaluesl,  &
      zel2re,  &
      order)
    diagl = 0.0E0_wp
    do i=1,m
      diagl(i,i) = eigenvaluesl(i)
    enddo

    diag = 0.0E0_wp
    do i=1,mn
      eps = abs(eigenvaluesr(i) - eigenvaluesr(i))
      if (eps > 1.0E-8_wp) then
        write(*,*) "Problems with eigenvalues."
        call exit
      else
        eigenvalues(i) = sqrt(eigenvaluesl(i))
        diag(i,i) = sqrt(eigenvaluesl(i))
      endif
    enddo

    ! Check if zer2re and zel2re orthogonal
    checkl = matmul(transpose(zel2re), zel2re)
    eps = abs(sum(checkl) - m)
    if (eps > 1.0E-10) then
      write(*,*) "Left transformation matrix not orthogonal."
      call exit
    endif
    checkr = matmul(transpose(zer2re), zer2re)
    eps = abs(sum(checkr) - n)
    if (eps > 1.0E-10) then
      write(*,*) "Right transformation matrix not orthogonal."
      call exit
    endif

    ! Fix signs using A ZR_i = ZL_i EV_i
    ! See: https://math.stackexchange.com/questions/2359992/how-to-resolve-the-sign-issue-in-a-svd-problem
    do i=1,n
      check(:,i) = matmul(matrix, zer2re(i,:)) / sqrt(eigenvaluesr(i))
    enddo
    do i=1,m
      ! If coloumn of check opposite in sign then row of zel2re
      !   -> change sign of row of zel2re
      if (abs(sum(check(:,i) - zel2re(i,:))) > 1.0E-10) then
        zel2re(i,:) = -zel2re(i,:)
      endif
    enddo

    ! Do not have to switch here because zel with M M^T
    zel = zel2re
    zer = zer2re

    ! check if get back original matrix
    check = abs(matmul(transpose(zel), matmul(diag, zer)) - matrix)
    eps = sum(check)
    if (eps > 1.0E-10_wp) then
      write(*,*) "SVD failed. Original matrix not recovered."
      call exit
    endif

    ! check if we get semi-diagonal matrix with singular values
    check = abs(matmul(zel, matmul(matrix, transpose(zer))) - diag)
    eps = sum(check)
    if (eps > 1.0E-10_wp) then
      write(*,*) "SVD failed. Transformation does not give semi-diagonal composition."
      call exit
    endif

  end subroutine svd

  subroutine svd_square(  &
    dimen,  &
    matrix,  &
    eigenvalues,  &
    zel,  &
    zer,  &
    kont,  &
    order)

    integer, intent(in) :: dimen
    real(wp), intent(in) :: matrix(dimen,dimen)
    real(wp), intent(out) :: eigenvalues(dimen)
    real(wp), intent(out) :: zel(dimen,dimen)
    real(wp), intent(out) :: zer(dimen,dimen)
    integer, intent(inout) :: kont
    integer, intent(inout) :: order

    real(wp) :: mat2(dimen,dimen)
    real(wp) :: zel2re(dimen,dimen)
    real(wp) :: zer2re(dimen,dimen)
    real(wp) :: eigenvalues2(dimen)
    integer :: i

    mat2 = matmul(transpose(matrix), matrix)
    call diagonalizerealsymmetric(  &
      mat2,  &
      eigenvalues2,  &
      zel2re,  &
      order)

    mat2 = matmul(matrix, transpose(matrix))
    call diagonalizerealsymmetric(  &
      mat2,  &
      eigenvalues2,  &
      zer2re,  &
      order)

    ! TB: Fix signs of zer2re
    mat2 = matmul(zer2re, matmul(matrix, transpose(zel2re)))
    do i=1,dimen
      if (mat2(i,i) < 0.0E0_wp) then
        zer2re(i,:) = -zer2re(i,:)
      endif
    enddo

    eigenvalues = sqrt(eigenvalues2)

    ! TB: Switch so that zer right and zel left
    zel = zer2re
    zer = zel2re

  end subroutine svd_square


  !---------------------------------------------------------------------
  ! Subroutine for diagonalization of complex hermitian matrices, based on the
  ! Householder algorithm. Is a portation of  EISCH1 to F90
  ! Input:
  !  Matrix ..... n times n matrix
  ! Output
  !  EigenValues ..... n sorted EigenValues: |m_1| < |m_2| < .. < |m_n|
  !  EigenVectors .... n times n matrix with the eigenvectors
  ! written by Werner Porod, 10.11.2000
  ! 19.07.02: adapting to multi precision
  !
  ! 12/2022: Modified by T. Biekotter
  !---------------------------------------------------------------------
  subroutine diagonalizehermitian(  &
    matrix,  &
    eigenvalues,  &
    eigenvectors,  &
    kont)

    complex(wp), intent(in) :: matrix(:,:)
    complex(wp), intent(out) :: eigenvectors(:,:)
    real(wp), intent(out) :: eigenvalues(:)
    integer, intent(inout) :: kont

    integer :: i1
    integer :: N1
    integer :: N2
    integer :: N3
    integer :: i2
    integer :: i3
    integer :: i4
    integer :: nrot
    real(wp) :: AbsAi
    real(wp) :: AbsTest
    real(wp), allocatable :: AR(:,:)
    real(wp), allocatable :: AI(:,:)
    real(wp), allocatable :: WR(:)
    real(wp), allocatable :: ZR(:,:)
    real(wp), allocatable :: work(:)
    real(wp), allocatable :: ZR_in(:,:)
    real(wp), allocatable :: testR(:,:)
    complex(wp), allocatable ::  test(:,:)
    complex(wp) :: IOne

    IOne = (0.0E0_wp, 1.0E0_wp)

    N1 = size(Matrix, Dim=1)
    N2 = size(eigenvalues)
    N3 = size(eigenvectors, Dim=1)

    allocate(AR(N1,N1))
    allocate(AI(N1,N1))
    allocate(Test(N1,N1))

    AbsAi = 0.0E0_wp
    AR = real(matrix)
    Ai = aimag(matrix)
    AbsAi = sum(abs(Ai))

    !--------------------------------------------------------------------------
    ! check first whether the matrix is really complex
    ! if not, I use the only real diagonalization because it is more accurate
    !--------------------------------------------------------------------------

    if (AbsAi < 1.0E-10_wp) then ! real matrix

      allocate(WR(N1))
      allocate(work(N1))
      allocate(ZR_in(N1,N1))
      allocate(testR(N1,N1))
      ZR_in = AR
      call jacobi(ZR_in, n1, n1, wr, ar, nrot)

      do n2=1,n1-1
        do n3=n2+1,n1
          if (abs(wr(n2)) > abs(wr(n3))) then
            work(1) = wr(n2)
            wr(n2) = wr(n3)
            wr(n3) = work(1)
            work = ar(:,n2)
            ar(:,n2) = ar(:,n3)
            ar(:,n3) = work
          end if
        enddo
      enddo

      eigenvalues = WR
      do i1=1,N1
        do i2=1,n2
          eigenvectors(i1,i2) = AR(i2,i1)
        enddo
      enddo

      do i1=1,n1
        do i2=1,n1
          testR(i1,i2) = 0.0E0_wp
          do i3=1,n1
            do i4=1,n1
              testR(i1,i2) = testR(i1,i2)  &
                + Ar(i3,i1) * ZR_in(i3,i4) * AR(i4,i2)
            enddo
          enddo
          AbsTest = absTest + Abs(testR(i1,i2))
        enddo
      enddo
      AbsTest = AbsTest / EigenValues(n1)

      deallocate(testR)

    else ! complex matrix

      allocate(ZR(2*N1,2*N1))
      allocate(ZR_in(2*N1,2*N1))
      allocate(WR(2*N1))
      allocate(work(2*N1))

      ZR_in(1:N1,1:N1) = AR
      ZR_in(N1+1:2*N1,N1+1:2*N1) = AR
      ZR_in(N1+1:2*N1,1:N1) = AI
      do i1=1,n1
        do i2=1,n1
         ZR_in(i1,N1+i2) = -AI(i1,i2)
        enddo
      enddo

      call jacobi(ZR_in, 2*n1, 2*n1, wr, zr, nrot)

!     do n2=1,2*n1-1
!       do n3=n2+1,2*n1
!         if (wr(n2) > wr(n3)) then
!           work(1) = wr(n2)
!           wr(n2) = wr(n3)
!           wr(n3) = work(1)
!           work = zr(:,n2)
!           zr(:,n2) = zr(:,n3)
!           zr(:,n3) = work
!         endif
!       enddo
!     enddo
      do n2=1,2*n1-1
        do n3=n2+1,2*n1
          if (kont == 1) then
            if (wr(n2) > wr(n3)) then
              work(1) = wr(n2)
              wr(n2) = wr(n3)
              wr(n3) = work(1)
              work = zr(:,n2)
              zr(:,n2) = zr(:,n3)
              zr(:,n3) = work
            endif
          else if (kont == -1) then
            if (wr(n2) < wr(n3)) then
              work(1) = wr(n2)
              wr(n2) = wr(n3)
              wr(n3) = work(1)
              work = zr(:,n2)
              zr(:,n2) = zr(:,n3)
              zr(:,n3) = work
            endif
          end if
        enddo
      enddo

      do i1=1,n1
        eigenvalues(i1) = wr(2*i1-1)
        do i2=1,n1
          eigenvectors(i1,i2) =  zr(i2,2*i1-1) - IOne * zr(n1+i2,2*i1-1)
        enddo
      enddo

!     do i1=2,n1
!       do i2=1,i1-1
!         work(1) = eigenvectors(i1,i2)
!         eigenvectors(i1,i2) = eigenvectors(i2,i1)
!         eigenvectors(i2,i1) = work(1)
!       enddo
!     enddo

      do i1=1,n1
        do i2=1,n1
          test(i1,i2) = 0.0E0_wp
          do i3=1,n1
            do i4=1,n1
              test(i1,i2) = test(i1,i2) &
                + eigenvectors(i1,i3) * matrix(i3,i4) * conjg(eigenvectors(i2,i4))
            enddo
          enddo
        enddo
      enddo

      eigenvectors = conjg(eigenvectors)

      deallocate(ZR)

    endif

    deallocate(AR, AI, WR, work, ZR_in)

  end subroutine diagonalizehermitian

  !---------------------------------------------------------------------
  ! Subroutine for diagonalization of real symmetric matrices, based on the
  ! Householder algorithm. Is a portation of  EISRS1 to F90
  ! Input:
  !  Matrix ..... n times n matrix
  ! Output
  !  EigenValues ..... n sorted EigenValues: |m_1| < |m_2| < .. < |m_n|
  !  EigenVectors .... n times n matrix with the eigenvectors
  ! written by Werner Porod, 11.11.2000
  !
  ! 12/22: Modifed by T. Biekotter
  !---------------------------------------------------------------------
  subroutine diagonalizerealsymmetric(  &
    matrix,  &
    eigenvalues,  &
    eigenvectors,  &
    order)

    real(wp), intent(in) :: matrix(:,:)
    real(wp), intent(out) :: eigenvectors(:,:)
    real(wp), intent(out)  :: eigenvalues(:)
    integer, intent(inout) :: order

    integer :: N1
    integer :: N2
    integer :: N3
    integer :: nrot
    real(wp), allocatable :: AR(:,:)
    real(wp), allocatable :: WR(:)
    real(wp), allocatable :: Work(:,:)

    integer :: dimen

    dimen = size(matrix, 1)

    N1 = size(matrix, dim=1)
    N2 = size(eigenvalues)
    N3 = size(eigenvectors, dim=1)

    allocate(AR(N1,N1))
    allocate(WR(N1))
    allocate(Work(N1,N1))

    Work = matrix

    call jacobi(Work, n1, n1, wr, ar, nrot)

    if (order == 0) then
      do n2=1,n1-1
        do n3=n2+1,n1
          if (abs(wr(n2)) > abs(wr(n3))) then
            work(1,1) = wr(n2)
            wr(n2) = wr(n3)
            wr(n3) = work(1,1)
            work(:,1) = ar(:,n2)
            ar(:,n2) = ar(:,n3)
            ar(:,n3) = work(:,1)
          endif
        enddo
      enddo
    else if (order == 1) then
      do n2=1,n1-1
        do n3=n2+1,n1
          if (abs(wr(n2)) < abs(wr(n3))) then
            work(1,1) = wr(n2)
            wr(n2) = wr(n3)
            wr(n3) = work(1,1)
            work(:,1) = ar(:,n2)
            ar(:,n2) = ar(:,n3)
            ar(:,n3) = work(:,1)
          endif
        enddo
      enddo
    endif

    eigenvalues = WR
    eigenvectors = transpose(AR)

    deallocate(AR, WR, Work)

  end subroutine diagonalizerealsymmetric

  subroutine jacobi(  &
    a,  &
    n,  &
    np,  &
    d,  &
    v,  &
    nrot)

    real(wp) :: a(np,np)
    integer :: n
    integer :: np
    real(wp) :: d(np)
    real(wp) :: v(np,np)
    integer :: nrot

    real(wp) :: Zero
    real(wp) :: Hundred
    real(wp) :: One
    real(wp) :: PointFive
    real(wp) :: PointTwo
    integer, parameter :: NMAX = 500
    integer :: i
    integer :: ip
    integer :: iq
    integer :: j
    real(wp) :: c
    real(wp) :: g
    real(wp) :: h
    real(wp) :: s
    real(wp) :: sm
    real(wp) :: t
    real(wp) :: tau
    real(wp) :: theta
    real(wp) :: tresh
    real(wp) :: b(NMAX)
    real(wp) :: z(NMAX)

    Zero = 0.0E0_wp
    Hundred = 100.0E0_wp
    One = 1.0E0_wp
    PointFive = 0.5E0_wp
    PointTwo = 0.2E0_wp

    do ip=1,n
      do iq=1,n
        v(ip,iq) = Zero
      enddo
      v(ip,ip) = One
    enddo

    do ip=1,n
      b(ip) = a(ip,ip)
      d(ip) = b(ip)
      z(ip) = Zero
    enddo

    nrot = 0

    do i=1,50

      sm = Zero
      do ip=1,n-1
        do iq=ip+1,n
          sm = sm + abs(a(ip,iq))
        enddo
      enddo

      if (sm == Zero) return

      if (i < 4) then
        tresh = PointTwo * sm / n**2
      else
        tresh = Zero
      endif

      do ip=1,n-1
        do iq=ip+1,n

          g = Hundred * abs(a(ip,iq))

          if ((i > 4) .and. ((abs(d(ip)) + g) == abs(d(ip)))  &
            .and. ((abs(d(iq)) + g) == abs(d(iq)))) then

            a(ip,iq) = Zero

          else if (abs(a(ip,iq)) > tresh) then

            h = d(iq) - d(ip)
            if ((abs(h) + g) == abs(h)) then
              t = a(ip,iq) / h
            else
              theta = PointFive * h / a(ip,iq)
              t = One / (abs(theta) + sqrt(One + theta**2))
              if (theta < Zero) t = -t
            endif

            c = One / sqrt(One + t**2)
            s = t * c
            tau = s / (One + c)
            h = t * a(ip,iq)
            z(ip) = z(ip) - h
            z(iq) = z(iq) + h
            d(ip) = d(ip) - h
            d(iq) = d(iq) + h
            a(ip,iq) = Zero

            do j=1,ip-1
              g = a(j,ip)
              h = a(j,iq)
              a(j,ip) = g - s * (h + g * tau)
              a(j,iq) = h + s * (g - h * tau)
            enddo

            do j=ip+1,iq-1
              g = a(ip,j)
              h = a(j,iq)
              a(ip,j) = g - s * (h + g * tau)
              a(j,iq) = h + s * (g - h * tau)
            enddo

            do j=iq+1,n
              g = a(ip,j)
              h = a(iq,j)
              a(ip,j) = g - s * (h + g * tau)
              a(iq,j) = h + s * (g - h * tau)
            enddo

            do j=1,n
              g = v(j,ip)
              h = v(j,iq)
              v(j,ip) = g - s * (h + g * tau)
              v(j,iq) = h + s * (g - h * tau)
            enddo

            nrot = nrot + 1

          endif

        enddo
      enddo

      do ip=1,n
        b(ip) = b(ip) + z(ip)
        d(ip) = b(ip)
        z(ip) = Zero
      enddo
    enddo

  end subroutine jacobi

end module linalg_matrices_diagonalize
