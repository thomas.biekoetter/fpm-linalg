module linalg_print_arrays

  use linalg_kinds, only : wp

  implicit none

  private

  public :: print_array
  public :: print_array_complex

contains

  subroutine print_array(x, newlines)

    real(wp), intent(in) :: x(:)
    integer, intent(in), optional :: newlines

    integer :: i
    integer :: n
    logical :: nl

    if (present(newlines)) then
      nl = newlines
    else
      nl = .true.
    end if

    n = size(x, dim=1)

    if (nl) then
      write(*,*)
    endif

    write(*,'(*(2X,4es11.3))') (x(i), i=1,n)

    if (nl) then
      write(*,*)
    endif

  end subroutine print_array

  subroutine print_array_complex(x, newlines)

    complex(wp), intent(in) :: x(:)
    integer, intent(in), optional :: newlines

    integer :: i
    integer :: n
    logical :: nl

    if (present(newlines)) then
      nl = newlines
    else
      nl = .true.
    end if

    n = size(x, dim=1)

    if (nl) then
      write(*,*)
    endif

    write(*,'(*(2X,4es11.3))') (x(i), i=1,n)

    if (nl) then
      write(*,*)
    endif

  end subroutine print_array_complex

end module linalg_print_arrays
