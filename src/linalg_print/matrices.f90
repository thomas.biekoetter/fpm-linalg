module linalg_print_matrices

  use linalg_kinds, only : wp

  implicit none

  private

  public :: print_matrix

contains

  subroutine print_matrix(x, newlines)

    real(wp), intent(in) :: x(:,:)
    integer, intent(in), optional :: newlines

    integer :: i
    integer :: j
    integer :: n
    integer :: m
    logical :: nl

    if (present(newlines)) then
      nl = newlines
    else
      nl = .true.
    end if

    m = size(x, dim=1)
    n = size(x, dim=2)

    if (nl) then
      write(*,*)
    endif

    do i=1,m
      write(*,'(*(2X,4es11.3))') (x(i,j), j=1,n)
    end do

    if (nl) then
      write(*,*)
    endif

  end subroutine print_matrix

end module linalg_print_matrices
