module linalg_checks_realness

  use linalg_kinds, only : wp

  implicit none

  private

  public :: isreal
  public :: isreal_array

  real(wp) :: epsmod = 1.0e-12_wp

contains

  function isreal(x, eps) result(y)

    complex(wp), intent(in) :: x
    real(wp), intent(in), optional :: eps
    logical :: y

    real(wp) :: e

    if (present(eps)) then
      e = eps
    else
      e = epsmod
    end if

    if (abs(aimag(x)) > e) then
      y = .false.
    else
      y = .true.
    end if

  end function isreal

  function isreal_array(x, eps) result(y)

    complex(wp), intent(in) :: x(..)
    real(wp), intent(in), optional :: eps
    logical :: y

    real(wp) :: e
    integer :: i
    integer :: s
    integer :: r
    complex(wp), allocatable :: xflat(:)
    complex(wp) :: xnum

    s = size(x)
    r = rank(x)

    allocate(xflat(s))

    ! Work around:
    !   https://fortran-lang.discourse.group/t/assumed-rank-arrays/1049
    select rank(x)
      rank(0) ; xnum = x
      rank(1) ; xflat = reshape(x, shape=[s])
      rank(2) ; xflat = reshape(x, shape=[s])
      rank(3) ; xflat = reshape(x, shape=[s])
      rank(4) ; xflat = reshape(x, shape=[s])
      rank(5) ; xflat = reshape(x, shape=[s])
      rank(6) ; xflat = reshape(x, shape=[s])
      rank default ; call exit
    end select

    if (present(eps)) then
      e = eps
    else
      e = epsmod
    end if

    if (r == 0) then
      y = isreal(xnum)
    else
      y = .true.
      do i=1,s
        if (.not. isreal(xflat(i))) then
          y = .false.
          exit
        end if
      end do
    end if

  end function isreal_array

end module linalg_checks_realness
