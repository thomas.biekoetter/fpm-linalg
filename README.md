# linalg

<h1 id="fortran-linalg"><img alt="linalg-Fortran" src="/media/logo.png" title="linalg-Fortran logo"></h1>

A light-weight Fortran package for linear algebra.

## Installation

The package is installed with the Fortran package manger fpm:
```
fpm build
```
Before compilation, the compiler and the compiler flags can
be set by defining the environment variables `FPM_FC` and
`FPM_FFLAGS`, respectively (see also the source files
`export_gfortran.sh` and `export_ifx.sh`).

Quad precision is activated by adding the flag `-DQUAD` to
the `FPM_FFLAGS` environment variable.

To use this package as a dependency in
another fpm project, add the following to the
fpm.toml file:
```
[dependencies]
linalg = { git="https://gitlab.com/thomas.biekoetter/fpm-linalg" }
```

## Test

The package has been developed and tested using
the gnu `gfortran` and the intel `ifx` compilers.

You can run the test routines with:
```
fpm test
```

## Example

An example program that uses the linalg package
can be found in the folder app. You can run
this program with:
```
fpm run
```

# License

The source code of `linalg`, related files and the documentation
are distributed under the MIT software license.
See the [LICENSE](https://gitlab.com/thomas.biekoetter/fpm-linalg/-/blob/master/LICENSE)
file for more details.

# Documentation

The [documentation] is still incomplete.

<!-- Links  -->
[documentation]: https://www.itp.kit.edu/~biekoetter/linalgdocu/site/
