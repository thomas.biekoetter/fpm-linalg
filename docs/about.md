This software is developed by
[Thomas Biekötter](mailto:thomas.biekoetter@desy.de),
and it is without warranty of any kind.

## License

This software is public under the
[MIT license](https://gitlab.com/thomas.biekoetter/fpm-linalg/-/blob/master/LICENSE).

