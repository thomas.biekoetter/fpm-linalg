<h1 id="fortran-linalg"><img alt="linalg-Fortran" src="logo.png" title="linalg-Fortran logo"></h1>

# <b>Welcome to fpm-linalg</b>

This is the documentation of the fpm
package <b>linalg</b>:
a light-weight Fortran package for linear algebra.

## <b>Installation</b>

The package is installed with the Fortran package manger
<a href="https://fpm.fortran-lang.org/" target="_blank">fpm</a>:
```
fpm build
```
Before compilation, the compiler and the compiler flags can
be set by defining the environment variables `FPM_FC` and
`FPM_FFLAGS`, respectively (see also the source files
`export_gfortran.sh` and `export_ifx.sh`).

Quad precision is activated by adding the flag `-DQUAD` to
the `FPM_FFLAGS` environment variable.

To use this package as a dependency in
another fpm project, add the following to the
fpm.toml file:
```
[dependencies]
linalg = { git="https://gitlab.com/thomas.biekoetter/fpm-linalg" }
```

## <b>Test</b>

The package has been developed and tested using
the gnu `gfortran` and the intel `ifx` compilers.

You can run the test routines with:
```
fpm test
```

## <b>Example</b>

An [example program](https://www.itp.kit.edu/~biekoetter/linalgdocu/site/example)
that uses the linalg package
can be found in the folder app. You can run
this program with:
```
fpm run
```
