## linalg_create
Contains functions and subroutines to create objects
with certain properties.

### linalg_create_arrays
Contains functions and subroutines to create arrays
with different shapes and properties.

#### Function: linspace
Returns a one-dimensional array
$y(num)$ with evenly spaced
numbers over the interval $[a,b]$.
``` fortran
function linspace(a, b, num) result(y)
  real(wp), intent(in) :: a
  real(wp), intent(in) :: b
  integer, intent(in) :: num
  real(wp), dimension(num) :: y
end function linspace
```

## linalg_matrices
Contains functions and routines for matrix operations.

### linalg_matrices_inverse
Contains functions to compute the inverse of a matrix.

#### Function: inversereal
Returns the inverse $a^{-1}$ of a real matrix $a$.
``` fortran
function inversereal(a) result(ainv)
  real(kind=wp), intent(in) :: a(:,:)
  real(kind=wp) :: ainv(size(a,1),size(a,2))
end function inversereal
```

#### Function: inversecomplex
Returns the inverse $a^{-1}$ of a complex matrix $a$.
``` fortran
function inversecomplex(a) result(ainv)
  complex(wp), intent(in) :: a(:,:)
  complex(wp) :: ainv(size(a,1),size(a,2))
end function inversecomplex
```
